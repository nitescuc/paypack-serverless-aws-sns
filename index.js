'use strict';

const ServerlessPluginHelper = require('@paypack/serverless-aws-helper');

class ServerlessPlugin extends ServerlessPluginHelper {
  constructor(serverless, options) {
    super(serverless, options);

    this.commands = {
      deploy: {
        usage: 'Create aws sns topic/trigger',
        lifecycleEvents: [
          'aws:info:displayFunctions'
        ],
      },
    };

    this.hooks = {
      'after:aws:info:displayFunctions': this.afterDisplayFunctions.bind(this),
    };
  }

  async afterDisplayFunctions() {
    this.serverless.cli.log('Creating SNS trigger');
    await this.eachEvent(async (functionObj, event) => {
      const FunctionName = functionObj.name;
      if (event.snsCustom) {
        let resp = await this.provider.request('SNS', 'createTopic', {
          Name: event.snsCustom
        });
        //
        const TopicArn = resp.TopicArn;
        await this.lambdaAddInvokePermission({
          FunctionName,
          Principal: 'sns.amazonaws.com',
          SourceArn: TopicArn
        });
        //
        const lambdaDesc = await this.provider.request('Lambda', 'getFunction', {
          FunctionName
        });
        const LambdaFunctionArn = lambdaDesc.Configuration.FunctionArn;
        // subscribe
        resp = await this.provider.request('SNS', 'listSubscriptionsByTopic', {
          TopicArn
        });
        if (!resp.Subscriptions.some((sub) => {
          return sub.TopicArn === TopicArn && sub.Endpoint === LambdaFunctionArn;
        })) {
          resp = await this.provider.request('SNS', 'subscribe', {
            TopicArn,
            Protocol: 'lambda',
            Endpoint: LambdaFunctionArn,
            ReturnSubscriptionArn: false
          });
          console.log('Creating subscription', resp);
        }
      }
    });
  }
}

module.exports = ServerlessPlugin;
